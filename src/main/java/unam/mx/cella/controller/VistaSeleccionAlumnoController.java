package unam.mx.cella.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.List;
import java.util.Locale;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManagerFactory;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import unam.mx.cella.modelo.Alumno;
import unam.mx.cella.modelo.AlumnoJpaController;
import unam.mx.cella.modelo.Categoria;
import unam.mx.cella.modelo.EntityProvider;
import unam.mx.cella.modelo.Subcategorias;
import unam.mx.cella.modelo.SubcategoriasJpaController;

/**
 *
 * @author rossa
 */
@ManagedBean
@ViewScoped
public class VistaSeleccionAlumnoController {

    private final EntityManagerFactory emf;
    private List<Alumno> alumnos;
    private Alumno seleccion;
    private Alumno alumno;
    private AlumnoJpaController ajpa; 
    private boolean valor;
    /**
     * Creates a new instance of VistaSeleccionController
     */
    public VistaSeleccionAlumnoController() {
        emf = EntityProvider.provider();
        FacesContext.getCurrentInstance().getViewRoot().setLocale(
                new Locale("es-Mx"));
        this.ajpa = new AlumnoJpaController(emf);
        this.alumnos = ajpa.findAlumnoEntities() ;
        this.seleccion = new Alumno();
    }
    
    public List<Alumno> getAlumnos(){
        return alumnos;
    }
    
    public void setAlumnos(List<Alumno> alumnos) {
        this.alumnos = alumnos;
    }
    
    public Alumno getSeleccion(){
        return seleccion;
    }
    
    public void setSeleccion(Alumno seleccion){
        this.seleccion = seleccion;
    }
     
    
    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Alumno Seleccionado", (((Alumno) event.getObject()).getId()).toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void onRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Alumno des-seleccionado", (((Alumno) event.getObject()).getId()).toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
         
        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    } 
    
    public boolean isValor(){
        return valor;
    }
    
    public void setValor(boolean valor){
        this.valor = valor;
    }
    
    public void bloquear() throws Exception{
        seleccion.setEdocuenta(valor);
        ajpa.edit(seleccion);
    }
    
}
