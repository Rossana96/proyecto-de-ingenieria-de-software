/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.mx.cella.controller;

import java.util.List;
import java.util.Locale;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManagerFactory;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import unam.mx.cella.modelo.EntityProvider;
import unam.mx.cella.modelo.Profesor;
import unam.mx.cella.modelo.ProfesorJpaController;

/**
 *
 * @author rossa
 */
@ManagedBean
@ViewScoped
public class VistaSeleccionProfesorController {

    private final EntityManagerFactory emf;
    private List<Profesor> profesores;
    private Profesor seleccion;
    private ProfesorJpaController pjpa; 
    private boolean valor;
    /**
     * Creates a new instance of VistaSeleccionController
     */
    public VistaSeleccionProfesorController() {
        emf = EntityProvider.provider();
        FacesContext.getCurrentInstance().getViewRoot().setLocale(
                new Locale("es-Mx"));
        this.pjpa = new ProfesorJpaController(emf);
        this.profesores = pjpa.findProfesorEntities() ;
        this.seleccion = new Profesor();
    }
    
    public List<Profesor> getProfesores(){
        return profesores;
    }
    
    public void setProfesores(List<Profesor> profesores) {
        this.profesores = profesores;
    }
    
    public Profesor getSeleccion(){
        return seleccion;
    }
    
    public void setSeleccion(Profesor seleccion){
        this.seleccion = seleccion;
    }
    
    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Profesor Seleccionado", (((Profesor) event.getObject()).getId()).toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void onRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Profesor des-seleccionado", (((Profesor) event.getObject()).getId()).toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public boolean isValor(){
        return valor;
    }
    
    public void setValor(boolean valor){
        this.valor = valor;
    }
    
    public void bloquear() throws Exception{
        seleccion.setEdocuenta(valor);
        pjpa.edit(seleccion);
    }
    
    
}
