/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.mx.cella.controller;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import unam.mx.cella.modelo.ContenerKitMaterial;
import unam.mx.cella.modelo.ContenerKitMaterialJpaController;
import unam.mx.cella.modelo.EntityProvider;
import unam.mx.cella.modelo.Material;
import unam.mx.cella.modelo.PertenecerMaterialCategoria;
import unam.mx.cella.modelo.PertenecerMaterialCategoriaJpaController;
import unam.mx.cella.modelo.Solicitarprestamoalumno;
import unam.mx.cella.modelo.SolicitarprestamoalumnoJpaController;
import unam.mx.cella.modelo.Unidadmaterial;
import unam.mx.cella.modelo.UnidadmaterialJpaController;
import unam.mx.cella.modelo.exceptions.IllegalOrphanException;
import unam.mx.cella.modelo.exceptions.NonexistentEntityException;

/**
 *
 * @author rossa
 */
@ManagedBean
@ViewScoped
public class VistaSeleccionMaterialController implements Serializable {

    
    private final EntityManagerFactory emf;
    private List<Unidadmaterial> materiales;
    private Unidadmaterial seleccion;
    private List<Unidadmaterial> seleccionados;
    private List<Unidadmaterial> filtrados;
    private UnidadmaterialJpaController umjpa;
    private SolicitarprestamoalumnoJpaController spajpa;
    private Integer id;
    private List<String> estados;
    private String estado;
    private List<Unidadmaterial> materialesDisponibles;
    private Integer disponibles;
    private Integer en_mantenimiento;
    private boolean disable;
    private String materialPrestado;
    private Integer idPrestado;
    private Solicitarprestamoalumno prestamo;
    private Date currentDate;
    /**
     * Creates a new instance of VistaSeleccionController
     */
    public VistaSeleccionMaterialController() {
        emf = EntityProvider.provider();
        FacesContext.getCurrentInstance().getViewRoot().setLocale(
                new Locale("es-Mx"));
        this.umjpa = new UnidadmaterialJpaController(emf);
        this.spajpa = new SolicitarprestamoalumnoJpaController(emf);
        this.materiales = umjpa.findUnidadmaterialEntities() ;
        this.seleccion = new Unidadmaterial();
        this.id = 0;
        this.estado = "";
        estados = new ArrayList<>();
        this.estados.add("en mantenimiento");
        this.estados.add("disponible");
        this.estados.add("no disponible");
        this.disponibles = 0;
        this.en_mantenimiento = 0;
        this.currentDate = new Date();
        this.materialPrestado = "";
        this.idPrestado = 0;
        this.prestamo = new Solicitarprestamoalumno();
        
    }
    
    public List<Unidadmaterial> getMateriales(){
        return materiales;
    }
    
    public void setMateriales(List<Unidadmaterial> materiales) {
        this.materiales = materiales;
    }
    
    public Unidadmaterial getSeleccion(){
        return seleccion;
    }
    
    public void setSeleccion(Unidadmaterial seleccion){
        this.seleccion = seleccion;
    }
    
    public List<Unidadmaterial> getSeleccionados(){
        return seleccionados;
    }
    
    public void setSeleccionados(List<Unidadmaterial> seleccionados){
        this.seleccionados = seleccionados;
    }
    
    public List<String> getEstados(){
        return estados;
    }
    
    public void setEstados(List<String> estados){
        this.estados = estados;
    }
    
    public String getEstado(){
        return estado;
    }
    
    public void setEstado(String estado){
        this.estado = estado;
    }
    
    public List<Unidadmaterial> getFiltrados(){
        return filtrados;
    }
    
    public void setFiltrados(List<Unidadmaterial> filtrados){
        this.filtrados = filtrados;
    }
    
    public String getMaterialPrestado(){
        return materialPrestado;
    }
    
    public void setMaterialPrestado(String materialPrestado){
        this.materialPrestado = materialPrestado;
    }
    
    public Integer getIdPrestado(){
        return idPrestado;
    }
    
    public void setIdPrtestado(Integer idPrestado){
        this.idPrestado = idPrestado;
    }
    
    public Solicitarprestamoalumno getPrestamo(){
        return prestamo;
    }
    
    public void setPrestamo(Solicitarprestamoalumno prestamo){
        this.prestamo =  prestamo;
    }
    
    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Material Seleccionado", (((Unidadmaterial) event.getObject()).getId()).toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void onRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Material des-seleccionado", (((Unidadmaterial) event.getObject()).getId()).toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void eliminar(ActionEvent e){
       
        materiales.remove(seleccion);
        seleccion = null;
        
    }
    
    public void cambiar(ActionEvent e) throws NonexistentEntityException, Exception{
       
        this.cambiarEdo();
        
    }
    
    public void eliminarMaterial() throws IllegalOrphanException, NonexistentEntityException {
        
        
        FacesContext fc = FacesContext.getCurrentInstance();
        this.id = Integer.parseInt(getIdParam(fc));
        umjpa.destroy(id);
        materiales.remove(seleccion);
        seleccion = null;
    }
    
    public void cambiarEdo() throws IllegalOrphanException, NonexistentEntityException, Exception {
    
        seleccion.setEstado(estado);
        umjpa.edit(seleccion);
        
    }
    
    public String getIdParam(FacesContext fc){
	Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
	return params.get("materialId");
		
    }
    
    public String getEdoParam(FacesContext fc){
        Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
	return params.get("materialEdo");
    }
    

    
    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
         
        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
        
    }
    
    public void detallePrestamo(ActionEvent actionEvent, Unidadmaterial material) throws NonexistentEntityException, Exception {
	material.setEstado("no disponible");
        umjpa.edit(material);	
        materialPrestado = material.getNombrematerial(); 
        idPrestado = material.getId();
        prestamo = spajpa.findByIdMaterial(material);
                
                
    }
    
  
    
    public String outcomeMaterial(){
        return "Material";
    }
    
    public String outcomeSubcategoria(){
        return "Subcategoria";
    }
    
    public String outcomeMatCa(){
        return "MaterialesdeCategoria";
    }
    
}
