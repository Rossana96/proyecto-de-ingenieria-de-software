package unam.mx.cella.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.List;
import java.util.Locale;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManagerFactory;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import unam.mx.cella.modelo.Administrador;
import unam.mx.cella.modelo.AdministradorJpaController;
import unam.mx.cella.modelo.EntityProvider;

/**
 *
 * @author rossa
 */
@ManagedBean
@ViewScoped
public class VistaSeleccionAdminController {

    private final EntityManagerFactory emf;
    private List<Administrador> admins;
    private Administrador seleccion;
    private AdministradorJpaController adminjpa;
    /**
     * Creates a new instance of VistaSeleccionAdminController
     */
    public VistaSeleccionAdminController() {
        emf = EntityProvider.provider();
        FacesContext.getCurrentInstance().getViewRoot().setLocale(
                new Locale("es-Mx"));
        this.adminjpa = new AdministradorJpaController(emf);
        this.admins = adminjpa.findAdministradorEntities() ;
        this.seleccion = new Administrador();
    }
    
    public List<Administrador> getAdmins(){
        return admins;
    }
    
    public void setAdmins(List<Administrador> admins) {
        this.admins = admins;
    }
    
    public Administrador getSeleccion(){
        return seleccion;
    }
    
    public void setSeleccion(Administrador seleccion){
        this.seleccion = seleccion;
    }
    
    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Admnistrador Seleccionado", (((Administrador) event.getObject()).getId()).toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void onRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Administradior des-seleccionado", (((Administrador) event.getObject()).getId()).toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
