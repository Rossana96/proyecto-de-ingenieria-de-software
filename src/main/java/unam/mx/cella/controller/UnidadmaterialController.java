/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.mx.cella.controller;

import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import unam.mx.cella.modelo.EntityProvider;
import unam.mx.cella.modelo.Solicitarprestamoalumno;
import unam.mx.cella.modelo.SolicitarprestamoalumnoJpaController;
import unam.mx.cella.modelo.Unidadmaterial;
import unam.mx.cella.modelo.UnidadmaterialJpaController;
import unam.mx.cella.modelo.exceptions.IllegalOrphanException;
import unam.mx.cella.modelo.exceptions.NonexistentEntityException;

/**
 *
 * @author rossa
 */
@ManagedBean
@RequestScoped
public class UnidadmaterialController {

    private final EntityManagerFactory emf;
    private Unidadmaterial unidadmaterial;
    private UnidadmaterialJpaController umjpa;
    private String estado;
    private Integer id;
    private Date currentDate;
    private String id1;
    Solicitarprestamoalumno prestamo;
    /**
     * Creates a new instance of UnidadmaterialController
     */
    public UnidadmaterialController() {
        emf = EntityProvider.provider();
        FacesContext.getCurrentInstance().getViewRoot().setLocale(
                new Locale("es-Mx"));
        this.unidadmaterial= new Unidadmaterial();
        this.estado = "";
        this.id = 0;
    }
    
    public Unidadmaterial getUnidadmaterial(){
        return unidadmaterial;
    } 
    
    public void setUnidadmaterial(Unidadmaterial unidadmaterial){
        this.unidadmaterial = unidadmaterial;
    }
    
    public String getEstado(){
        return estado;
    }
    
    public void setEstado(String estado){
        this.estado = estado;
    }
    
    public String getId(){
        return id1;
    }
    
    public void setId(String id){
        this.id1 = id;
    }
    
    public Solicitarprestamoalumno getPrestamo(){
        return prestamo;
    }
    
    public String getIdParam(FacesContext fc){
	Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
	return params.get("materialId");
		
    }
    
    public String getEdoParam(FacesContext fc){
        Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
	return params.get("materialEdo");
    }
    
    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }
    
    public void eliminarUnidad() throws IllegalOrphanException, NonexistentEntityException{
        FacesContext fc = FacesContext.getCurrentInstance();
        this.id = Integer.parseInt(getIdParam(fc));
        umjpa = new UnidadmaterialJpaController(emf);
        umjpa.destroy(id);
        FacesContext.getCurrentInstance().addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                               "El material con el id "+ id + " fue eliminado correctamente" , ""));
    }
    
    
    public void cambiarEdoUnidad() throws NonexistentEntityException, Exception{
        FacesContext fc = FacesContext.getCurrentInstance();
        this.estado = getEdoParam(fc);
        unidadmaterial.setEstado(estado);
        umjpa.edit(unidadmaterial);
        umjpa = new UnidadmaterialJpaController(emf);
        umjpa.destroy(unidadmaterial.getId());
        FacesContext.getCurrentInstance().addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                               "El material con el id "+ unidadmaterial.getId() + " fue actualizado correctamente" , ""));
    }
    
    public void prestarUnidad() throws NonexistentEntityException, Exception{
        FacesContext fc = FacesContext.getCurrentInstance();
        this.id = Integer.parseInt(getIdParam(fc));
        umjpa = new UnidadmaterialJpaController(emf);
        prestamo = new Solicitarprestamoalumno();
        SolicitarprestamoalumnoJpaController spajpa = new SolicitarprestamoalumnoJpaController(emf); 
        Unidadmaterial material = umjpa.findUnidadmaterial(id);
        prestamo.setIdUnidadMaterial(material);
        currentDate = new Date();
        prestamo.setFechainicio(currentDate);
        spajpa.create(prestamo);
        FacesContext.getCurrentInstance().addMessage(null,
                                            new FacesMessage(FacesMessage.SEVERITY_INFO,
                                               "Solcitud de prestamo registrada con el id: " + prestamo.getId() , ""));
        FacesContext.getCurrentInstance().getExternalContext().redirect("DatosPrestamo.xhtml");
    }
    
    private EntityManager getEntityManager() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
