/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.mx.cella.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManagerFactory;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import unam.mx.cella.modelo.EntityProvider;
import unam.mx.cella.modelo.Subcategorias;
import unam.mx.cella.modelo.SubcategoriasJpaController;

/**
 *
 * @author rossa
 */
@ManagedBean
@ViewScoped
public class VistaSeleccionSubcategoriasController implements Serializable {

    
    private final EntityManagerFactory emf;
    private List<Subcategorias> subcategorias;
    private Subcategorias seleccion;
    private SubcategoriasJpaController scjpa;
    private Integer id;
    /**
     * Creates a new instance of VistaSeleccionController
     */
    public VistaSeleccionSubcategoriasController() {
        emf = EntityProvider.provider();
        FacesContext.getCurrentInstance().getViewRoot().setLocale(
                new Locale("es-Mx"));
        this.scjpa = new SubcategoriasJpaController(emf);
        this.subcategorias = scjpa.findSubcategoriasEntities() ;
        this.seleccion = new Subcategorias();
    }
    
    public List<Subcategorias> getSubcategorias(){
        return subcategorias;
    }
    
    public void setSubcategorias(List<Subcategorias> subcategorias) {
        this.subcategorias = subcategorias;
    }
    
    public Subcategorias getSeleccion(){
        return seleccion;
    }
    
    public void setSeleccion(Subcategorias seleccion){
        this.seleccion = seleccion;
    }
    
    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Subcategoria Seleccionada", (((Subcategorias) event.getObject()).getId()).toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void onRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Subcategoria des-seleccionada", (((Subcategorias) event.getObject()).getId()).toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    
    
    public Integer getId(){
        return id;
    }
    
     public void setId(Integer productId) {
        this.id = id;
    }
}
