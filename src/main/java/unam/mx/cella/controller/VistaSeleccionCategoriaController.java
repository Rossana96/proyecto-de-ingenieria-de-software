/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.mx.cella.controller;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManagerFactory;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import unam.mx.cella.modelo.Categoria;
import unam.mx.cella.modelo.CategoriaJpaController;
import unam.mx.cella.modelo.EntityProvider;
import unam.mx.cella.modelo.PertenecerMaterialCategoria;
import unam.mx.cella.modelo.PertenecerMaterialCategoriaJpaController;
import unam.mx.cella.modelo.Subcategorias;
import unam.mx.cella.modelo.SubcategoriasJpaController;

/**
 *
 * @author rossa
 */
@ManagedBean
@ViewScoped
public class VistaSeleccionCategoriaController implements Serializable {

    
    private final EntityManagerFactory emf;
    private List<Categoria> categorias;
    private Categoria seleccion;
    private CategoriaJpaController cjpa; 
    private SubcategoriasJpaController scjpa;
    private PertenecerMaterialCategoriaJpaController pmcjpa;
    private List<SelectItem> cat;  
    private List<PertenecerMaterialCategoria> relacion;
    private List<Subcategorias> subcategorias;
    private Integer id;
    
    /**
     * Creates a new instance of VistaSeleccionController
     */
    public VistaSeleccionCategoriaController() {
        emf = EntityProvider.provider();
        FacesContext.getCurrentInstance().getViewRoot().setLocale(
                new Locale("es-Mx"));
        this.pmcjpa = new PertenecerMaterialCategoriaJpaController(emf);
        this.cjpa = new CategoriaJpaController(emf);
        this.scjpa = new SubcategoriasJpaController(emf);
        this.categorias = cjpa.findCategoriaEntities() ;
        this.subcategorias = scjpa.findSubcategoriasEntities();
        this.seleccion = new Categoria();
        this.cat = new ArrayList<>();
        this.relacion = pmcjpa.findPertenecerMaterialCategoriaEntities();
        this.id = 0;
    }
    
    public List<Categoria> getCategorias(){
        return categorias;
    }
    
    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }
    
    public Categoria getSeleccion(){
        return seleccion;
    }
    
    public Integer getId(){
	
	return id;
		
    }

    
    
    public List<PertenecerMaterialCategoria> getRelacion(){
         return relacion;
     }
     
     public void setRelacion(List<PertenecerMaterialCategoria> relacion){
         this.relacion = relacion;
     }
    
    public void setSeleccion(Categoria seleccion){
        this.seleccion = seleccion;
    }
    
    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Categoria Seleccionada", (((Categoria) event.getObject()).getId()).toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void onRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Categoria des-seleccionada", (((Categoria) event.getObject()).getId()).toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    
    
    
}
