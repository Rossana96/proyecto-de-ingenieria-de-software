/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.mx.cella.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.faces.application.FacesMessage;
import javax.persistence.EntityManagerFactory;
import org.primefaces.event.FlowEvent;
import unam.mx.cella.modelo.ContenerKitMaterial;
import unam.mx.cella.modelo.ContenerKitMaterialJpaController;
import unam.mx.cella.modelo.Kit;
import unam.mx.cella.modelo.EntityProvider;
import unam.mx.cella.modelo.KitJpaController;
import unam.mx.cella.modelo.Material;
import unam.mx.cella.modelo.MaterialJpaController;

/**
 *
 * @author rossa
 */
@ManagedBean
@ApplicationScoped
public class KitController {

    private KitJpaController kjpa;
    private ContenerKitMaterial ckm;
    private ContenerKitMaterialJpaController ckmjpa;
    private Kit kit;
    private final EntityManagerFactory emf;
    private MaterialJpaController mjpa;
    private Material material;
    private boolean skip;
    private Integer id;
    private List<Kit> kits;
    private String nombreKit;
    /**
     * Creates a new instance of CrearKitController
     * @return 
     */
    
    public Kit getKit(){
        return kit;
    }
    
    public void setKit(Kit kit) {
        this.kit = kit;
    }
    
    public Material getMaterial(){
        return material;
    }
    
    public void setMaterial(Material material){
        this.material = material;
    }
    
    public String getNombreKit(){
        return nombreKit;
    }
    
    public void setNombreKit(String nombreKit){
        this.nombreKit = nombreKit;
    }
    
    public KitController() {
         emf = EntityProvider.provider();
        
        FacesContext.getCurrentInstance().getViewRoot().setLocale(
                new Locale("es-Mx"));
        this.kit = new Kit();
        this.kjpa = new KitJpaController(emf);
        this.mjpa = new MaterialJpaController(emf);
        this.ckm = new ContenerKitMaterial();
        this.ckmjpa = new ContenerKitMaterialJpaController(emf);
        this.material = new Material ();
    }
    
    public Kit verificaKit(String nombreKit){
        
        KitJpaController ajpa = new KitJpaController(emf);
        return ajpa.verificaNombre(nombreKit);
    }
    
     
     
    public String addKit() throws IOException {
        
        if(verificaKit(kit.getNombrekit())!= null){
            FacesContext.getCurrentInstance().addMessage(null
            , new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "Intenta con otro nombre, el kit con el nombre" + this.kit.getNombrekit()+ " ya existe", ""));
        } 
             
        else {
            
            KitJpaController kjpa = new KitJpaController(emf);
            Kit nkit = new Kit();
           
            nkit.setNombrekit(kit.getNombrekit());
            nkit.setMateria(kit.getMateria());
           // nkit.setIdProfesor(InicioSesion.prof);

            kjpa.create(nkit);
            this.kit = verificaKit(kit.getNombrekit());
           /* FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Felicidades, el registro se ha realizado correctamente", ""));*/
              FacesContext.getCurrentInstance().getExternalContext().redirect("SeleccionarMateriales.xhtml");
             
        }
        return null;
    }
    
    public boolean isSkip() {
        return skip;
    }
 
    public void setSkip(boolean skip) {
        this.skip = skip;
    }
    
    public Integer getId(){
        return id;
    }
    
    public List<Kit> getFiltrados(Integer id){
        List<Kit> filtrados = new ArrayList<>();
        this.kits = kjpa.findKitEntities() ;
        for(Kit k:kits){
            if(k.getIdProfesor().getId().equals(id)){
                filtrados.add(k);
            }
        }
        return filtrados;
    }
    
    public void llenarKit() throws IOException{
        
        ckm.setNombrekit(kit.getNombrekit());
        ckm.setNombrematerial(mjpa.findMaterial(material.getId()).getNombrematerial());
        ckm.setIdMaterial(mjpa.findMaterial(material.getId()));
        ckmjpa.create(ckm);
        FacesContext.getCurrentInstance().getExternalContext().redirect("Kits.xhtml");
    }
    
    public void crearKit() throws IOException{
        
            if(!((kjpa.verificaNombre(kit.getNombrekit()))==null)){
            FacesContext.getCurrentInstance().addMessage(null
            , new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "Intenta con otro nombre, el kit con el nombre" + this.kit.getNombrekit()+ " ya existe", ""));
            } 
             
            else {
                kit.setNombrekit(kit.getNombrekit());
                kit.setMateria(kit.getMateria());
                kjpa.create(kit);
                
            }
            
            FacesContext.getCurrentInstance().getExternalContext().redirect("LlenarKits.xhtml");
        
    }
    
    
    public String onFlowProcess(FlowEvent event) {
        if(skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        }
        else {
            return event.getNewStep();
        }
        
    }
    
}