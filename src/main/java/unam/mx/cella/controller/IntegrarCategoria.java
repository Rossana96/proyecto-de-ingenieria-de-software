/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.mx.cella.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManagerFactory;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DualListModel;
import unam.mx.cella.modelo.Categoria;
import unam.mx.cella.modelo.CategoriaJpaController;
import unam.mx.cella.modelo.EntityProvider;
import unam.mx.cella.modelo.Material;
import unam.mx.cella.modelo.MaterialJpaController;
import unam.mx.cella.modelo.PertenecerMaterialCategoria;
import unam.mx.cella.modelo.PertenecerMaterialCategoriaJpaController;
import unam.mx.cella.modelo.Subcategorias;
import unam.mx.cella.modelo.SubcategoriasJpaController;
import unam.mx.cella.modelo.Unidadmaterial;
import unam.mx.cella.modelo.UnidadmaterialJpaController;

/**
 *
 * @author rossa
 */
@ManagedBean
@ViewScoped
public class IntegrarCategoria implements Serializable  {

    private Categoria categoria;
    private Subcategorias subcategoria;
    private String c;
    private String s;
    private final CategoriaJpaController cjpa;
    private final SubcategoriasJpaController scjpa;
    private final MaterialJpaController mjpa;
    private Material material;
    private EntityManagerFactory emf;
    private List<Categoria> categorias;
    private List<Subcategorias> subcategorias;
    private List<Material> materiales ;
    private List<Material> seleccionados;
    private DualListModel<Material> m;
    private final PertenecerMaterialCategoria pmc;
    private List<Subcategorias> filtrados;
    private boolean skip;
    private Integer id;
    /**
     * Creates a new instance of IntegrarCategoria
     */
    public IntegrarCategoria() {
        emf = EntityProvider.provider();
        FacesContext.getCurrentInstance().getViewRoot().setLocale(
                new Locale("es-Mx"));
        this.c = "";
        this.s = "";        
        this.categoria = new Categoria();
        this.subcategoria = new Subcategorias();
        this.material = new Material();
        this.cjpa = new CategoriaJpaController(emf);
        this.scjpa = new SubcategoriasJpaController(emf);
        this.mjpa = new MaterialJpaController(emf);
        this.pmc = new PertenecerMaterialCategoria();
        this.categorias = cjpa.findCategoriaEntities();
        this.subcategorias = scjpa.findSubcategoriasEntities();
        this.materiales = mjpa.findMaterialEntities();
        this.seleccionados = new ArrayList<>();
        this.m = new DualListModel<>((List<Material>)materiales,(List<Material>)seleccionados);
        this.skip = false;
        this.id = 0;
    }
    
    public String getC(){
       return c;
    }
    
    public void setC(String c){
        this.c = c;
    }
    
    public String getS(){
       return s;
    }
    
    public void setS(String s){
        this.s = s;
    }
    
    public Categoria getCategoria(){
       return categoria;
    }
    
    public void setCatgoria(Categoria categoria){
        this.categoria = categoria;
    }
    
    public Subcategorias getSubcategoria(){
        return subcategoria;
    }
    
    public void setSubcategoria(Subcategorias subcategoria){
        this.subcategoria = subcategoria;
    }
    
    public Material getMaterial(){
        return material;
    }
    
    public void setNombrematerial(Material material){
        this.material = material;
    }
    
    public Integer getId(){
        return id;
    }
    
    public void setId(Integer id){
        this.id = id;
    }
    
    public List<Categoria> getCategorias(){
       return categorias;
    }
    
    public void setCatgorias(List<Categoria> categorias){
        this.categorias = categorias;
    }
    
    public List<Subcategorias> getSubcategorias(){
        return subcategorias;
    }
    
    public void setSubcategorias(List<Subcategorias> subcategorias){
        this.subcategorias = subcategorias;
    }
    
     public List<Material> getMateriales(){
       return materiales;
    }
    
    public void setMateriales(List<Material> materiales){
        this.materiales = materiales;
    }
    
    public List<Material> getSeleccionados(){
       return seleccionados;
    }
    
    public void setSeleccionados(List<Material> seleccionados){
        this.seleccionados = seleccionados;
    }
    
    public DualListModel<Material> getM(){
       return m;
    }
    
    public void setM(DualListModel<Material> m){
        this.m = m;
    }
    
    
    
    public String getIdParam(FacesContext fc){
	Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
	return params.get("materialId");
		
    }
    
    public String getSubParam(FacesContext fc){
	Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
	return params.get("subcategoria");
		
    }
    
    public boolean isSkip() {
        return skip;
    }
 
    public void setSkip(boolean skip) {
        this.skip = skip;
    }
    
    public void crearRelacion(){
            pmc.setNombrecategoria(categoria.getNombrecategoria());
            pmc.setNombrematerial(mjpa.findMaterial(id).getNombrematerial());
            pmc.setIdCategoria(cjpa.findCategoria(categoria.getNombrecategoria()));
            pmc.setIdMaterial(mjpa.findMaterial(id));
            emf = EntityProvider.provider();
            PertenecerMaterialCategoriaJpaController pmcc = new PertenecerMaterialCategoriaJpaController(emf);
            pmcc.create(pmc);
        
        
    }
    
    
    public List<Subcategorias> getFiltrados() {
        
        filtrados = new ArrayList<>();
         
        for (Subcategorias s:subcategorias) {
            if((s.getNombrecategoria().equals((cjpa.findCategoria(categoria.getNombrecategoria())).getNombrecategoria()))){
                filtrados.add(s);
            }
        }
         
        return filtrados;
    }
    
    public void onTransfer(TransferEvent event) {
        StringBuilder builder = new StringBuilder();
        for(Object item : event.getItems()) {
            builder.append(((Material) item).getNombrematerial()).append("<br />");
        }
         
        FacesMessage msg = new FacesMessage();
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        msg.setSummary("Items Transferred");
        msg.setDetail(builder.toString());
         
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }  
     
    public void onSelect(SelectEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Item Selected", event.getObject().toString()));
    }
     
    public void onUnselect(UnselectEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Item Unselected", event.getObject().toString()));
    }
     
    public void onReorder() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "List Reordered", null));
    }
    
    public String onFlowProcess(FlowEvent event) {
        if(skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        }
        else {
            return event.getNewStep();
        }
        
    }
    
    
    
    
    
    
    
    
    
    
}
