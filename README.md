# Cella
Autor Rossana Palma López, Pablo Martínez Yessica Janeth, Martínez	López Jaime Alberto


Ejecucion del poyecto...

Para poder ejecutar el proyecto es necesario intalar previamente la siguiente version de Netbeans:  https://netbeans.org/downloads/

![](netbeans.gif)

Debemos intalar la verion que se señala arriba, ya que es la que incluye Apache Tomcat 8.0, recurso sumamente importanmte en el proyecto.

Hecha la debida instalacion procedemos a clonar el Proyecto:

https://gitlab.com/Rossana96/proyecto-de-ingenieria-de-software.git

En este punto debemos intalar Postgresql en nuestras computadoras:

https://www.postgresql.org/download/

Abrimos Pgadmin4 y realizamos lo siguiente:

1.- Creamos un nuevo rol en Postgresql
![](pgadmin4-role.gif)
Podemos cambiar los permisos como mas nos parezca conveniente

2.- Creamos una nueva base de datos 
![](pgadmin4.gif)
En este caso yo elegi el rol que previamente cree

3.- Creamos un nuevo schema en nuestra base de datos
![](pgadmin4-schema.gif)

4.- Subimos el script que se encuentra en el repositorio llamado alternativa.sql, esto atraves de la opcion Query Tool
![](pgadmin4-query.gif)

5.-Finalmente revisamos si  existen tablas en nuestro schema, de ser asi, habremos terminado de configurar Postgresql
![](pgadmin4-tables.gif)

Hecho esto, abrimos el archivo persistance.xml de nuestro proyecto, que se encuentra en la carpeta resources 

\src\main\resources\META-INF

y modificamos las lineas siguientes

<property name="javax.persistence.jdbc.user" value="aquivaturol"/>
<property name="javax.persistence.jdbc.password" value="aquivatucontraseña"/>

esto para no tener problemas al hacer querys a nuestra base datos.

Procedemos a abrir Netbeans, una vez abierto nuetros proyecto y despues de haber dado clic derecho sobre el y seleccionar run maven, corremos el proyecto de la siguente manera
![](CorrerProyecto.gif)

Esperamos a que se abra el proyecto web y navegamos de forma intuitiva.

*Nota : Para ingresar como administrador tendras que crear un  script en tu schema como el que sigue:

INSERT INTO cella.administrador (nombreusuario, correo, contrasena, nombre,  apellidoP,  apellidoM, rfc) VALUES ('admin' 'tucorreo@ciencias.unam.mx', 12345678', 'tunombre', 'tuapelliop', 'tuapellidom', ''1234567890');

GRACIAS POR LEERME.


